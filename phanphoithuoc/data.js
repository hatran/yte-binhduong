var data = [
 {
   "STT": 2,
   "Name": "Bệnh viện Đa khoa tỉnh Bình Dương",
   "address": "05, Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Bình Dương",
   "Longtitude": 10.997657,
   "Latitude": 106.655243
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Y học cổ truyền tỉnh Bình Dương",
   "address": "02 Yersin, Phú Cường, TP.Thủ Dầu Một Tỉnh Bình Dương",
   "Longtitude": 10.98161,
   "Latitude": 106.657487
 },
 {
   "STT": 4,
   "Name": "Ban Bảo vệ Sức khoẻ cán bộ tỉnh Bình Dương",
   "address": "5  Phạm Ngọc Thạch, Hiệp Thành, TP.Thủ Dầu Một, Tỉnh Bình Dương",
   "Longtitude": 10.996825,
   "Latitude": 106.655893
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Điều dưỡng - Phục hồi chức năng tỉnh Bình Dương",
   "address": "31 Yersin, Phú Cường, TP.Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.983436,
   "Latitude": 106.663594
 },
 {
   "STT": 6,
   "Name": "Trung tâm chăm sóc sức khỏe sinh sản",
   "address": "213 Yersin, TP.Thủ Dầu Một, Tỉnh Bình Dương",
   "Longtitude": 10.900708,
   "Latitude": 106.764903
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Đa khoa Cao su Dầu Tiếng",
   "address": "Kp4, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, tỉnh Bình Dương",
   "Longtitude": 11.282139,
   "Latitude": 106.363541
 },
 {
   "STT": 8,
   "Name": "Trung tâm Y tế thành phố Thủ Dầu Một",
   "address": "201 Cách Mạng Tháng Tám, Phú Cường, Thủ Dầu Một, Bình Dương",
   "Longtitude": 10.975523,
   "Latitude": 106.658146
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Đa khoa huyện Dĩ An",
   "address": "500 ĐT 743, Ấp Đông Tác, Xã Tân Đông Hiệp, Huyện Dĩ An, Tỉnh Bình Dương.",
   "Longtitude": 10.91031,
   "Latitude": 106.78158
 },
 {
   "STT": 10,
   "Name": "Trung tâm Y tế thị xã Thuận An",
   "address": "Nguyễn Văn Tiết, Lái Thiêu, Thị xã  Thuận An, Bình Dương",
   "Longtitude": 10.908702,
   "Latitude": 106.701927
 },
 {
   "STT": 11,
   "Name": "Trung tâm Y tế thị xã Bến Cát",
   "address": "QL13, Mỹ Phước, Bến Cát, tỉnh Bình Dương",
   "Longtitude": 11.161296,
   "Latitude": 106.594363
 },
 {
   "STT": 12,
   "Name": "Trung tâm Y tế thị xã Bắc Tân Uyên",
   "address": "Thị xã Tân Uyên, Tỉnh Bình Dương",
   "Longtitude": 11.149371,
   "Latitude": 106.844631
 },
 {
   "STT": 13,
   "Name": "Trung tâm Y tế Phú Giáo",
   "address": "Huyện Phú Giáo, Tỉnh Bình Dương",
   "Longtitude": 11.289096,
   "Latitude": 106.797492
 },
 {
   "STT": 14,
   "Name": "Trung tâm Y tế huyện Dầu Tiếng",
   "address": "Kp5, Thị trấn Dầu Tiếng, Huyện Dầu Tiếng, Bình Dương",
   "Longtitude": 11.287301,
   "Latitude": 106.393155
 },
 {
   "STT": 15,
   "Name": "Trung tâm Y tế huyện Thuận An",
   "address": "Nguyễn Văn Tiõt, Thị trấn Lái Thiêu, Thị xã thuận An, tỉnh Bình Dương",
   "Longtitude": 10.908709,
   "Latitude": 106.701943
 },
 {
   "STT": 16,
   "Name": "Trung tâm Y tế thị xã Thủ Dầu Một",
   "address": "Phú Cường, Thành phố Thủ Dầu Một, tỉnh Bình Dương",
   "Longtitude": 10.965357,
   "Latitude": 106.666735
 },
 {
   "STT": 17,
   "Name": "Trung tâm Y tế huyện Bến Cát",
   "address": "Kp5, Thị trấn Mỹ Phước, Huyện Bến Cát, Bình Dương",
   "Longtitude": 11.161317,
   "Latitude": 106.594374
 },
 {
   "STT": 18,
   "Name": "Trung tâm Y tế huyện Tân Uyên (Bệnh viện Đa khoa thị xã Tân Uyên)",
   "address": "Khu 4, Thị trấn Uyên Hưng, Huyện Tân Uyên, Bình Dương",
   "Longtitude": 11.070097,
   "Latitude": 106.792849
 },
 {
   "STT": 19,
   "Name": "Trung tâm Y tế huyện Phú Giáo",
   "address": "Thị trấn Phước Vĩnh, Huyện Phú Giáo, Bình Dương",
   "Longtitude": 11.289099,
   "Latitude": 106.797508
 }
];